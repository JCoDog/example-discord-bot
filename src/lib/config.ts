import dotenv from "dotenv";

dotenv.config();

// Extract the required env vars from the process
const { DISCORD_TOKEN, DISCORD_CLIENT_ID } = process.env;

// Check for missing env vars and throw error
if (!DISCORD_TOKEN || !DISCORD_CLIENT_ID) {
	throw new Error("Missing environment variables");
}

// Exports the config object so you can use env vars in the discord bot, do not console log or return these variables anywhere in the application.
export const config = {
	DISCORD_TOKEN,
	DISCORD_CLIENT_ID,
};
