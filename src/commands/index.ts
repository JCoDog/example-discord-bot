import {
	CacheType,
	ChatInputCommandInteraction,
	InteractionResponse,
	Message,
	SlashCommandBuilder,
	SlashCommandOptionsOnlyBuilder,
	SlashCommandSubcommandBuilder,
	SlashCommandSubcommandGroupBuilder,
	SlashCommandSubcommandsOnlyBuilder,
} from "discord.js";
import Ping from "./utility/Ping";
import Say from "./utility/Say";

export type TCommand = {
	data: SlashCommandBuilder | SlashCommandOptionsOnlyBuilder | SlashCommandSubcommandsOnlyBuilder | SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder;
	execute: (
		interaction: ChatInputCommandInteraction<CacheType>
	) => Promise<InteractionResponse<boolean>> | Promise<Message<boolean>>;
};

// Declare the list of commands for the bot by importing them and exporting them in the commands object
const commands = {
	Ping,
	Say,
};

export default commands;
