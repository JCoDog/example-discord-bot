import {
	ChatInputCommandInteraction,
	PermissionFlagsBits,
	SlashCommandBuilder,
} from "discord.js";
import { TCommand } from "..";

export const data = new SlashCommandBuilder()
	.setName("say")
	.setDescription("Says what you tell it to")
	.setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages)
	.addStringOption((option) =>
		option
			.setName("message")
			.setDescription("The message to send")
			.setRequired(true)
	);

export async function execute(interaction: ChatInputCommandInteraction) {
	const message = await interaction.options.getString("message");

	interaction.reply({
		content: "Sending your message...",
		ephemeral: true,
	});

	await interaction.channel?.send({
		content: message!,
	});

	return interaction.editReply({
		content: "Message sent.",
	});
}

const Say: TCommand = {
	data,
	execute,
};

export default Say;
