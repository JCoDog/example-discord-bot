import {
	ChatInputCommandInteraction,
	EmbedBuilder,
	SlashCommandBuilder,
} from "discord.js";
import { TCommand } from "..";

export const data = new SlashCommandBuilder()
	.setName("ping")
	.setDescription("Replies with Pong!");

export async function execute(interaction: ChatInputCommandInteraction) {
	const timestamp = `<t:${Math.floor(Date.now() / 1000)}:R>`;

	const pongEmbed = new EmbedBuilder()
		.setColor("Blurple")
		.setTitle("You ping, I pong.")
		.setDescription(
			"You tested if the bot is responding with our handy ping command."
		)
		.addFields({
			name: "Pinged",
			value: `${timestamp}`,
		})
		.setTimestamp();

	return interaction.reply({ content: "Pong!", embeds: [pongEmbed] });
}

const Ping: TCommand = {
	data,
	execute,
};

export default Ping;
