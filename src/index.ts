import { Client } from "discord.js";
import { deployCommands } from "./lib/utility/deploy-commands";
import commands from "./commands";
import { config } from "@/lib/config";

const client = new Client({
	intents: ["Guilds", "GuildMessages", "DirectMessages"],
});

// Let yourself know that your bot is online and ready to be used
client.once("ready", () => {
	console.log("Discord bot is ready!");
});

// Deploy the commands to any guild that adds the bot
client.on("guildCreate", async (guild) => {
	await deployCommands({ guildId: guild.id });
});

// Handle slash commands being used
client.on("interactionCreate", async (interaction) => {
	if (!interaction.isCommand()) return;

	if (!interaction.isChatInputCommand()) return;

	console.log("running command " + interaction.commandName);

	let { commandName } = interaction;

	function capitalizeFirstLetter(string: string) {
		return string[0].toUpperCase() + string.slice(1);
	}

	commandName = capitalizeFirstLetter(commandName);

	if (commands[commandName as keyof typeof commands]) {
		commands[commandName as keyof typeof commands].execute(interaction);
	}
});

// Login to the bot
client.login(config.DISCORD_TOKEN);
